# -*- coding: Utf-8 -*-

import csv
import networkx as nx
import random
import datetime
import matplotlib.pyplot as plt

##########################################################
# Lit un fichier .CSV et rempli un graphe en cons�quence #
##########################################################
def readCsv(filePath): 
    with open(filePath, 'r', encoding='UTF8') as csvfile:
        readCSV = csv.DictReader(csvfile, delimiter=',')

        graph = nx.Graph()
        streetIndex = 0

        for street in readCSV:
            marueInEdge = street['COMMUNE'] + " " + street['LIBELLE']

            if(street["TENANT"] != '' and street["ABOUTISSANT"] != ''): 
			#TENANT ET ABOUTISSANT
				graph.add_edge(street["TENANT"], street["ABOUTISSANT"], rue=marueInEdge, weight=street["BI_MAX"], pheromone=0)
			elif(street["TENANT"] == '' and street["ABOUTISSANT"] != ''):
			#PAS DE TENANT ET ABOUTISSANT
                graph.add_edge(street["COMMUNE"] +" "+ str(i), street["ABOUTISSANT"], rue=marueInEdge, weight=street["BI_MAX"], pheromone=0)
            elif(street["TENANT"] != '' and street["ABOUTISSANT"] == ''):
			#TENANT ET PAS D'ABOUTISSANT
                graph.add_edge(street["TENANT"], street["COMMUNE"] +" "+ str(i), rue=marueInEdge, weight=street["BI_MAX"], pheromone=0)
            elif(street["TENANT"] == '' and street["ABOUTISSANT"] == ''):
			#AUCUN TENANT NI ABOUTISSANT
                graph.add_edge(street["COMMUNE"] + " " + str(i), street["COMMUNE"] +" "+ str(i + 1), rue=marueInEdge, weight=street["BI_MAX"], pheromone=0)
            i = i + 1

        return graph
   
##############################################################
# 		Gestion de la prochaine action de la fourmi		     #
##############################################################
def findNextStop(graph, graphLink, ant):

    start = ant['start']
    end = ant['end']

    # R�cup�ration de la rue de d�part
    index = graphLink[start]
    currentStreet = ""

    # On boucle jusqu'� trouver l'arriv�e
    while(end != currentStreet):
        
		# randomization
        r = random.randint(0,1)
        node = graph.edges()[index][r]

        choice = fit(graph.edges(node, data='rue'))

        currentStreet = graph.edges(node, data='rue')[choice][2]['rue']

        index = graphLink[currentStreet]
        
        if(index in ant["passage"]): # Si pas encore emprunt� cette route
            remainingChoices = []
			
            # v�rification du nombre de possibilit�s restantes
            for remainingStreets in graph.edges(node, data='rue'):
                indextempo = graphLink[remainingStreets[2]['rue']]
                if(indextempo in ant["passage"]):
                    remainingChoices.append(remainingStreets)

            # il reste des choix : on en choisi un au hasard parmi les possibilit�s
            if(len(remainingChoices)>0):
                choice = fit(graph.edges(remainingChoices[0][0], data='rue'))
                currentStreet = graph.edges(node, data='rue')[choice][2]['rue']
                index = graphLink[currentStreet]

        # Sauvegarde en cache des passages effectu�s
        ant["passage"][index] = index
        if(graph.edges(noeud, data='rue')[choice][2]['weight'] != ''):
            ant["distance"] = ant["distance"] + (int(graph.edges(noeud, data='rue')[choice][2]['weight'])*0.01)
        
    if(end == currentStreet):
        print("ok")
        return ant

###################################
# Fonction de d�p�t de ph�romones #
###################################
def layDownPheromones(graph, ant):

    path = ant["passage"]
    for row in path:
        score = 0
        if(ant["distance"] > 1):

            lk = str(1 / ant["distance"]).split('.')
			#print("---->" + lk)
            score = 0

            for lettre in lk[1]:
                if(lettre == '0'):
                    score = score + 10
                else:
                    score = score + int(lettre)
                    break
            if(score <= 1000): # on utilisera ici une base de 1000 pour le scoring
                score = 1000 - score
            else:              # balancing si jamais on est dans le n�gatif
                score = 0
        graph.edges(data='pheromone')[row][2]['pheromone'] = graph.edges(data='pheromone')[row][2]['pheromone'] + score

    return graph

#########################################
# Fonction de suppression de ph�romones #
#########################################
def removePheromones(graph):
	# On supprime un petit peu de ph�ronomones partout
    for i in range(len(graph.edges())):
        if(graph.edges(data='pheromone')[i][2]['pheromone'] > 0):
            graph.edges(data='pheromone')[i][2]['pheromone'] = round(graph.edges(data='pheromone')[i][2]['pheromone'] - (graph.edges(data='pheromone')[i][2]['pheromone'] * 0.05))
    return graph


def fit(nodes):
    # Cela sera la valeur max du random a faire
    maxRank = 0
    rank = 1000/len(nodes)
    ranks = []
	
    # ecriture des plages de rank
    for node in nodes:
        # on ajoute les pheromonemones
        if(node[2]['pheromone'] > 0):
            ranks.append(rank + node[2]['pheromone'])
            maxRank = maxRank + rank + node[2]['pheromone']
        else:
            # sinon on prend la rank normal
            ranks.append(rank)
            maxRank = maxRank + rank

    # On randomise sur tous ca
    monRAN = random.randint(0, round(maxRank))
    
    # Index du tableau et index du noeud choisie
    i = 0
    mintest = 0
    maxtest = 0
    for rank in ranks:
        maxtest = maxtest + rank
        if(monRAN >= mintest and monRAN <= maxtest):
            return i
        i = i + 1
        mintest = mintest + rank
    return -1
		
##############
# CONSTANTES #
##############
ANTS = 5
GENERATIONS = 2

#################################
#								#
#			TEST LAB			#
#								#
#################################
def init():
    graph = readCsv('VOIES_NM.csv')
    
	i = 0
    graphLink = {}

    for street in list(graph.edges_iter(data='rue')):
        varsar = street[2]['rue']
        graphLink[varsar] = i
        i = i + 1
    
	#################################
	
    for generation in range(GENERATIONS):
        for ant in range(ANTS):

			newAnt = {} # NOUVELLE FOURMIE
			newAnt["name"] = "nom"+ant
			newAnt["distance"] = 0
			newAnt["start"] = "BRAINS Voie Non D�nomm�e 0240895"
			newAnt["end"] = "BRAINS Route de la Pilaudi�re"
			newAnt["passage"] = {}

            newAnt = findNextStop(graph, graphLink, newAnt)
			
            print("D�pot des ph�romones")

            graph = layDownpheromonemones(graph, newAnt)
            graph = removepheromonemones(graph)
			
			print("Disparition des ph�romones")
		
# Run auto
if __name__ == "__main__":
init()